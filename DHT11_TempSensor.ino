/* Solution: DHT11_TempSensor, DHT11_TempSensor.ino
 * Measure temperature and humidity with the DHT11 Sensor with the Arduino Uno
 * 03.11.2018
 * Kai Sackl
 */

#include "LiquidCrystal.h"
#include "DHT.h"

#define DHTPIN 7
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);
LiquidCrystal lcd(A0, A1, A2, A3, A4, A5);

// The setup() function runs once each time the micro-controller starts
void setup()
{
	setBuildInLED(LOW);
	lcd.begin(16, 2);
	Serial.begin(9600);
}

// Add the main program code into the continuous loop() function
void loop()
{
	delay(2000);
	float humidity = dht.readHumidity();
	float temperature = dht.readTemperature();

	if (isnan(humidity) || isnan(temperature)) {
		Serial.println("Failed to read from sensor!");
		return;
	}

	Serial.println("Luftfeuchtigkeit: ");
	Serial.println(humidity);
	Serial.println("Temperatur: ");
	Serial.println(temperature);

	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("Temp: ");
	lcd.print(temperature);
	lcd.print("C");

	lcd.setCursor(0, 1);
	lcd.print("Luftfeu.: ");
	lcd.print(humidity);
	lcd.print("%");
}

void setBuildInLED(uint8_t highLow)
{
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, highLow);
}